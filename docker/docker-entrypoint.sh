#!/bin/bash

# THIS FILE ALWAYS NEEDS TO BE ENCODED WITH LF, NOT CRLF
# IF YOU SEE 'standard_init_linux.go:190: exec user process caused "no such file or directory"' YOUR LINE ENDINGS ARE WRONG!

echo "env:"
echo "=========================================="

port="${IMPULSE_PORT:-5000}"
sed -i -e "s/%PORT%/${port}/g" swarm.toml
sed -i -e "s/%PORT%/${port}/g" daemon.toml
sed -i -e "s/%PORT%/${port}/g" client.toml
echo "PORT=${port}"

name="${IMPULSE_NAME:-5000}"
sed -i -e "s/%NAME%/${name}/g" swarm.toml
sed -i -e "s/%NAME%/${name}/g" daemon.toml
sed -i -e "s/%NAME%/${name}/g" client.toml
echo "NAME=${name}"

mode="${IMPULSE_MODE:-daemon}"
echo "MODE=${mode}"

echo "=========================================="
echo
echo "MODE=${mode}"
if [ "$mode" == "daemon" ]; then
  echo "daemon.toml: "
  echo "=========================================="
  cat daemon.toml
  echo
  echo "=========================================="
  echo
  echo "starting daemon..."
  java -jar impulse-daemon.jar
elif [ "$mode" == "swarm" ]; then
  echo "swarm.toml: "
  echo "=========================================="
  cat swarm.toml
  echo
  echo "=========================================="
  echo
  echo "starting swarm..."
  java -jar impulse-swarm.jar
elif [ "$mode" == "client" ]; then
  echo "client.toml: "
  echo "=========================================="
  cat client.toml
  echo
  echo "=========================================="
  echo
  echo "starting client..."
  java -jar impulse-client.jar
else
  echo "UNKNWON MODE ${mode}"
fi
