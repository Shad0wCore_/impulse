/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.client;

import io.impulsecloud.core.CoreBootstrap;
import io.impulsecloud.core.util.ValueConverters;

import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Impulse client bootstrap implementation
 *
 * @version 1.0
 * @since 1.0
 */
public class ClientBootstrap extends CoreBootstrap {

    public static void main(String[] args) {
        new ClientBootstrap(args);
    }

    private OptionSpec<Path> configFileOption;

    public ClientBootstrap(String[] args) {
        super(args);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configureOptionParser() {
        super.configureOptionParser();

        configFileOption = this.optionParser.accepts("configFile", "Specifies the config file that should be used")
            .withRequiredArg().withValuesConvertedBy(ValueConverters.PATH);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean bootstrap(OptionSet optionSet) {
        if (super.bootstrap(optionSet)) {
            return true;
        }

        Path configFilePath = optionSet.valueOf(configFileOption);
        if (configFilePath == null) {
            configFilePath = Paths.get("client.toml");
        }

        new ClientRuntimeInitializer(configFilePath).initialize();

        return true;
    }

}
