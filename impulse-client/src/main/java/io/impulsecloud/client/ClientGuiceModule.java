/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.client;

import io.impulsecloud.core.ImpulseGuiceModule;
import io.impulsecloud.core.network.AtomixConnector;

public class ClientGuiceModule extends ImpulseGuiceModule {

    private ClientConfig config;

    public ClientGuiceModule(ClientConfig config) {
        this.config = config;
    }

    @Override
    protected void configure() {
        super.configure();

        bind(AtomixConnector.class).to(ClientAtomixConnector.class).asEagerSingleton();

        bind(ClientConfig.class).toInstance(config);
    }

}
