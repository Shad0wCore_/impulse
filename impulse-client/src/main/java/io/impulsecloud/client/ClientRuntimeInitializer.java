/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.client;

import io.impulsecloud.client.cli.CLIThread;
import io.impulsecloud.core.CoreRuntimeInitializer;

import com.google.inject.Injector;
import java.nio.file.Path;
import javax.inject.Inject;

/**
 * Runtime initializer for the client
 *
 * @version 1.0
 * @since 1.0
 */
public class ClientRuntimeInitializer extends CoreRuntimeInitializer {

    @Inject
    private ClientAtomixConnector atomixConnector;
    @Inject
    private CLIThread cliThread;

    private Path configPath;
    private ClientConfig config;

    public ClientRuntimeInitializer(Path configPath) {
        this.configPath = configPath;
    }

    @Override
    public void loadConfig() {
        config = configParser.load(configPath, ClientConfig.class);

        replaceDefaultUUID(config, config.getNetworkConfig(), configPath);
    }

    @Override
    public Injector createInjector() {
        return new ClientGuiceModule(config).createInjector();
    }

    @Override
    protected void initAtomix() {
        atomixConnector.connect(config.getNetworkConfig());
    }

    @Override
    public void initialize() {
        super.initialize();

        cliThread.start();
    }
}
