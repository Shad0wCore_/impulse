/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.client.cli;

import io.impulsecloud.client.ClientRuntimeInitializer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.inject.Inject;
import javax.inject.Singleton;

//TODO replace with TCA
@Singleton
public class CLIThread extends Thread {

    private static final Logger LOG = LogManager.getLogger(ClientRuntimeInitializer.class);

    @Inject
    private Commands commands;

    @Override
    public synchronized void start() {
        super.start();
        setContextClassLoader(Thread.currentThread().getContextClassLoader());
        setName("ImpulseCLIThread");
    }

    @Override
    public void run() {
        commands.registerCommands();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] cmd = line.split(" ");

                commands.execute(cmd);
            }
        } catch (IOException ex) {
            LOG.error("Error while handling CLI input", ex);
        }
    }
}
