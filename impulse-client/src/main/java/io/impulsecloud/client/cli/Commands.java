/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.client.cli;

import io.impulsecloud.client.ClientAtomixConnector;
import io.impulsecloud.core.network.data.AtomixNetworkMember;

import com.google.inject.Injector;
import io.atomix.cluster.Member;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Commands {

    private static final Logger LOG = LogManager.getLogger(Commands.class);

    @Inject
    private ClientAtomixConnector atomixConnector;
    @Inject
    private Injector injector;

    private Map<String, Consumer<String[]>> commands = new HashMap<>();

    public void execute(String[] cmd) {
        if (commands.containsKey(cmd[0])) {
            try {
                commands.get(cmd[0]).accept(cmd);
            } catch (Exception e) {
                LOG.error("Error while executing command: '" + cmd[0] + "'", e);
            }
        } else {
            LOG.warn("Unknown command: '{}'", cmd[0]);
        }
    }

    public void registerCommands() {
        commands.put("stop", (cmd) -> System.exit(0));
        commands.put("members", (cmd) -> {
            if (atomixConnector.getAtomix() == null) {
                LOG.warn("Not connected yet!");
                return;
            }

            var nodes = atomixConnector.getAtomix().<AtomixNetworkMember>getSet("Nodes").async();
            var msg = nodes.stream().map(AtomixNetworkMember::toString).collect(Collectors.joining("\n\t"));
            LOG.info("Connected Nodes: \n\t" + msg);
        });
    }
}
