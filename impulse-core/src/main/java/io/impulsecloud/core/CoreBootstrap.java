/*
 *
 *  * Copyright (c) 2018, Impulse Cloud and its contributors
 *  *
 *  * This code is licensed under the MIT license found in the
 *  * LICENSE file in the root directory of this source tree.
 *
 */

package io.impulsecloud.core;

import io.impulsecloud.core.util.ManifestUtil;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Core bootstrap implementation of the Impulse runtime
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class CoreBootstrap {

    private static final Logger LOG = LogManager.getLogger(CoreBootstrap.class);

    protected OptionParser optionParser;

    /**
     * Starts the bootstrapping process by starting to process the command line arguments
     *
     * @param args the command line arguments
     */
    public CoreBootstrap(String[] args) {
        // Create OptionParser instance
        optionParser = new OptionParser();
        // Adding accepted options
        configureOptionParser();

        OptionSet optionSet;
        try {
            // Parsing options to a OptionSet
            optionSet = optionParser.parse(args);
        } catch (OptionException ex) {
            LOG.error(ex.getMessage());
            return;
        }

        // Bootstrapping
        if (!bootstrap(optionSet)) {
            printHelp();
        }
    }

    /**
     * Configures the option parser with the accepted options
     */
    public void configureOptionParser() {
        this.optionParser.accepts("version", "Prints out the version");
    }

    /**
     * Does the concrete bootstrapping based on the parsed arguments
     *
     * @return false if the arguments couldn't be handled and help should be printed
     */
    public boolean bootstrap(OptionSet optionSet) {
        if (optionSet.has("version")) {
            LOG.info("You are running Impulse v{}", ManifestUtil.getImplementationVersion());
            return true;
        }

        return false;
    }

    /**
     * Prints the help page to standard out
     */
    public void printHelp() {
        try {
            this.optionParser.printHelpOn(System.out);
        } catch (IOException ex) {
            LOG.error("Error while printing help", ex);
        }
    }

}
