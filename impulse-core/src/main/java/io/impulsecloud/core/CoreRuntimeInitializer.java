/*
 *
 *  * Copyright (c) 2018, Impulse Cloud and its contributors
 *  *
 *  * This code is licensed under the MIT license found in the
 *  * LICENSE file in the root directory of this source tree.
 *
 */

package io.impulsecloud.core;

import io.impulsecloud.core.network.AtomixConfig;
import io.impulsecloud.core.util.ExceptionFilter;
import io.impulsecloud.core.util.ManifestUtil;
import io.impulsecloud.core.util.config.ConfigParser;

import com.google.inject.Injector;
import io.netty.channel.ConnectTimeoutException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Core runtime initializer implementation<br> Used to do initializing after the arguments have been parsed
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class CoreRuntimeInitializer {

    private static final UUID DEFAULT_UUID = UUID.fromString("0000-00-00-00-000000");
    private static final Logger LOG = LogManager.getLogger(CoreRuntimeInitializer.class);

    protected ConfigParser configParser;

    /**
     * Initializes the software
     */
    public void initialize() {
        LOG.info("Starting Impulse v{}...", ManifestUtil.getImplementationVersion());

        // use a temp injector to get the config parser
        Injector tempInjector = new ImpulseGuiceModule().createInjector();
        configParser = tempInjector.getInstance(ConfigParser.class);

        // init config, needs to be before guice so that config can be provided to guice
        loadConfig();

        // init guice
        Injector injector = createInjector();
        injector.injectMembers(this);

        configureLogging();

        initAtomix();
    }

    /**
     * Creates the injector that will be used for the impulse instance
     *
     * @return the injector
     */
    public abstract Injector createInjector();

    /**
     * Loads the config
     */
    public abstract void loadConfig();

    public void replaceDefaultUUID(Object config, AtomixConfig networkConfig, Path configPath) {
        // generate a random uuid if needed
        if (DEFAULT_UUID.equals(networkConfig.getUuid())) {
            LOG.info("Found default uuid, generating a new one...");
            networkConfig.setUuid(UUID.randomUUID());

            try {
                configParser.save(configPath, config);
            } catch (IOException ex) {
                LOG.error("Failed saving configuration to \"" + configPath + "\": ", ex);
            }
        }
    }

    /**
     * Inits atomix
     */
    protected abstract void initAtomix();

    protected void configureLogging() {
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        Configuration config = ctx.getConfiguration();
        LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);

//        List<String> excludes = new ArrayList<>();
//        excludes.add(".*direct buffer constructor: unavailable.*");
//        excludes.add(".*jdk.internal.misc.Unsafe.allocateUninitializedArray\\(int\\)*");
//
//        String regex = excludes.stream().collect(Collectors.joining(")||(","(",")"));
//        System.out.println(regex);
//        try {
//            loggerConfig
//                .addFilter(RegexFilter.createFilter(regex, new String[0], false, Result.DENY, Result.NEUTRAL));
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }

        // filter out way to verbose exceptions
        Map<Class<? extends Throwable>, String> excludes = new HashMap<>();
        excludes.put(UnsupportedOperationException.class, "Reflective setAccessible");
        excludes.put(IllegalAccessException.class, "jdk.internal.misc.Unsafe");
        excludes.put(ConnectTimeoutException.class, "connection timed out");
        excludes.put(IOException.class, "Eine vorhandene Verbindung");
        try {
            //noinspection unchecked
            excludes.put(
                (Class<? extends Throwable>) Class
                    .forName("io.netty.channel.AbstractChannel$AnnotatedConnectException"), "Connection refused");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        loggerConfig.addFilter(new ExceptionFilter(excludes));
    }
}
