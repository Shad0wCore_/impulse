/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core;

import io.impulsecloud.core.blueprint.Blueprints;
import io.impulsecloud.core.blueprint.registry.BlueprintTypeRegistry;
import io.impulsecloud.core.blueprint.registry.ImpulseBlueprintTypeRegistry;
import io.impulsecloud.core.util.config.ConfigParser;
import io.impulsecloud.core.util.config.TomlConfigParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * AbstractModule implementation binding Impulse classes to Guice
 *
 * @version 1.0
 * @since 1.0
 */
public class ImpulseGuiceModule extends AbstractModule {

    public Injector createInjector() {
        return Guice.createInjector(this);
    }

    @Override
    protected void configure() {
        super.configure();

        bind(BlueprintTypeRegistry.class).to(ImpulseBlueprintTypeRegistry.class);

        bind(ConfigParser.class).to(TomlConfigParser.class).asEagerSingleton();

        bind(Gson.class).toInstance(getGson());

        requestStaticInjection(Blueprints.class);
    }

    protected Gson getGson() {
        return new GsonBuilder().create();
    }

}
