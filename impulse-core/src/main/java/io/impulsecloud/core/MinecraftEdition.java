package io.impulsecloud.core;

public enum MinecraftEdition {

    JAVA_EDITION,
    BEDROCK_EDITION

}
