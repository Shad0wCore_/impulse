package io.impulsecloud.core.event;

public interface Cancelable {

    boolean isCancelled();

    void setCancelled(boolean cancelled);

}
