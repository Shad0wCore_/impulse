package io.impulsecloud.core.event.handler;

import io.impulsecloud.core.event.Event;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

public abstract class EventHandler<T extends Event> {

    private final Class<T> handlingEventClass;

    public EventHandler(Class<T> handlingEventClass) {
        this.handlingEventClass = handlingEventClass;
    }

    protected abstract boolean handle(T event);

    @SuppressWarnings("unchecked")
    public final void post(Event event) {
        Preconditions.checkNotNull(event, "Passed event is referring to a null reference");
        Preconditions.checkArgument(this.handlingEventClass.isAssignableFrom(event.getClass()),
                                    "Cannot handle incompatible event class: Expected {}; Got {}",
                                    this.handlingEventClass.getName(),
                                    event.getClass().getName());

        this.handle((T) event);
    }

    public Class<T> getHandlingEventClass() {
        return handlingEventClass;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(handlingEventClass);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventHandler<?> that = (EventHandler<?>) o;
        return Objects.equal(handlingEventClass, that.handlingEventClass);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("handlingEventClass", handlingEventClass)
            .toString();
    }

}
