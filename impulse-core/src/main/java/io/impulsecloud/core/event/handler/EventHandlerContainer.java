package io.impulsecloud.core.event.handler;

import io.impulsecloud.core.event.Event;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

import java.util.Collection;

public final class EventHandlerContainer<T extends Event> {

    private Class<T> handlingEventClass;
    private Collection<EventHandler<T>> eventHandlers;

    public EventHandlerContainer(Class<T> handlingEventClass, Collection<EventHandler<T>> eventHandlers) {
        this.handlingEventClass = handlingEventClass;
        this.eventHandlers = eventHandlers;
    }

    public void post(T event) {
        Preconditions.checkNotNull(event, "Passed event is referring to a null reference");
        Preconditions.checkArgument(this.handlingEventClass.isAssignableFrom(event.getClass()),
                                    "Cannot handle incompatible event class: Expected {}; Got {}",
                                    this.handlingEventClass.getName(),
                                    event.getClass().getName());

        this.getEventHandlers().forEach(eventHandler -> eventHandler.post(event));
    }

    public Class<T> getHandlingEventClass() {
        return this.handlingEventClass;
    }

    public Collection<EventHandler<T>> getEventHandlers() {
        return this.eventHandlers;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(handlingEventClass, eventHandlers);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventHandlerContainer<?> that = (EventHandlerContainer<?>) o;
        return Objects.equal(handlingEventClass, that.handlingEventClass)
            && Objects.equal(eventHandlers, that.eventHandlers);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("handlingEventClass", handlingEventClass)
            .add("eventHandlers", eventHandlers)
            .toString();
    }

}
