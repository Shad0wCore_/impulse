package io.impulsecloud.core.event.handler;

import io.impulsecloud.core.event.Event;

import java.util.Collection;
import java.util.Optional;

public interface EventHandlerRegistry {

    void registerHandlers(EventHandler<? extends Event>[] eventHandlers);

    void registerHandlers(Collection<EventHandler<? extends Event>> eventHandlers);

    <T extends Event> void registerHandler(EventHandler<T> eventHandler);

    void unregisterHandlers(Collection<EventHandler<? extends Event>> eventHandlers);

    void unregisterHandler(EventHandler<? extends Event> eventHandler);

    <T extends Event> Optional<EventHandlerContainer<T>> getContainer(Class<T> eventClass);

    Collection<EventHandler<? extends Event>> getHandlers(Class<? extends Event> eventClass);

}
