package io.impulsecloud.core.event.handler;

import io.impulsecloud.core.event.Event;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public class SimpleEventHandlerRegistry implements EventHandlerRegistry {

    private final Map<Class<? extends Event>, EventHandlerContainer<?>> eventListeners = Maps.newHashMap();

    @Override
    public void registerHandlers(EventHandler<? extends Event>[] eventHandlers) {
        this.registerHandlers(Arrays.asList(eventHandlers));
    }

    @Override
    public void registerHandlers(Collection<EventHandler<? extends Event>> eventHandlers) {
        eventHandlers.forEach(this::unregisterHandler);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Event> void registerHandler(EventHandler<T> eventHandler) {
        var handlingEventClass = eventHandler.getHandlingEventClass();
        EventHandlerContainer<T> container = (EventHandlerContainer<T>) this.eventListeners.get(handlingEventClass);

        if (Optional.ofNullable(container).isPresent()) {
            container.getEventHandlers().add(eventHandler);
            eventListeners.put(handlingEventClass, container);
        } else {
            var handlers = Lists.newArrayList(eventHandler);
            eventListeners.put(handlingEventClass, new EventHandlerContainer<>(handlingEventClass, handlers));
        }
    }

    @Override
    public void unregisterHandlers(Collection<EventHandler<? extends Event>> eventHandlers) {
        eventHandlers.forEach(this::unregisterHandler);
    }

    @Override
    public void unregisterHandler(EventHandler<? extends Event> eventHandler) {
        Optional<EventHandlerContainer<? extends Event>> eventListener = Optional
            .ofNullable(this.eventListeners.get(eventHandler.getHandlingEventClass()));

        eventListener.ifPresent(container -> container.getEventHandlers().remove(eventHandler));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Event> Optional<EventHandlerContainer<T>> getContainer(Class<T> eventClass) {
        EventHandlerContainer<T> found = (EventHandlerContainer<T>) this.eventListeners.get(eventClass);
        return Optional.ofNullable(found);
    }


    @Override
    public Collection<EventHandler<? extends Event>> getHandlers(Class<? extends Event> eventClass) {
        var foundEventHandlers = new ArrayList<EventHandler<?>>();
        this.getContainer(eventClass).ifPresent(container -> foundEventHandlers.addAll(container.getEventHandlers()));

        return foundEventHandlers;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(eventListeners);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimpleEventHandlerRegistry that = (SimpleEventHandlerRegistry) o;
        return Objects.equal(eventListeners, that.eventListeners);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("eventListeners", eventListeners)
            .toString();
    }

}
