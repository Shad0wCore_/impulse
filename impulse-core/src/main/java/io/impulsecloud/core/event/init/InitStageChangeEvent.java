package io.impulsecloud.core.event.init;

import io.impulsecloud.core.event.Event;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class InitStageChangeEvent extends Event {

    private String previousStage;
    private String nextStage;

    public InitStageChangeEvent(String previousStage, String nextStage) {
        super("impulse:event:stage_change");

        this.previousStage = previousStage;
        this.nextStage = nextStage;
    }

    public String getPreviousStage() {
        return previousStage;
    }

    public String getNextStage() {
        return nextStage;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(previousStage, nextStage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InitStageChangeEvent that = (InitStageChangeEvent) o;
        return Objects.equal(previousStage, that.previousStage)
            && Objects.equal(nextStage, that.nextStage);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("previousStage", previousStage)
            .add("nextStage", nextStage)
            .toString();
    }

}
