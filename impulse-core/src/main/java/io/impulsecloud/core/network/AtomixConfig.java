/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.network;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.util.List;
import java.util.UUID;

/**
 * Holds the configuration options for atomix
 *
 * @version 1.0
 * @since 1.0
 */
public class AtomixConfig {

    private UUID uuid;
    private int port;
    private String bindAddress;
    private List<SwarmNode> initialSwarmNodes;

    public AtomixConfig(UUID uuid, int port, String bindAddress, List<SwarmNode> initialSwarmNodes) {
        this.uuid = uuid;
        this.port = port;
        this.bindAddress = bindAddress;
        this.initialSwarmNodes = initialSwarmNodes;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getBindAddress() {
        return bindAddress;
    }

    public void setBindAddress(String bindAddress) {
        this.bindAddress = bindAddress;
    }

    public List<SwarmNode> getInitialSwarmNodes() {
        return initialSwarmNodes;
    }

    public void setInitialSwarmNodes(List<SwarmNode> initialSwarmNodes) {
        this.initialSwarmNodes = initialSwarmNodes;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("uuid", uuid)
            .add("port", port)
            .add("bindAddress", bindAddress)
            .add("initialSwarmNodes", initialSwarmNodes)
            .toString();
    }
}
