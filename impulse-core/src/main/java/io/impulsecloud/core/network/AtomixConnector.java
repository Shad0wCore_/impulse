/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.network;

import io.impulsecloud.core.network.data.AtomixNetworkMember;

import io.atomix.cluster.Member;
import io.atomix.cluster.MemberId;
import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;
import io.atomix.core.profile.Profile;
import io.atomix.primitive.Consistency;
import io.atomix.primitive.Replication;
import io.atomix.primitive.protocol.ProxyProtocol;
import io.atomix.protocols.backup.MultiPrimaryProtocol;
import io.atomix.protocols.raft.MultiRaftProtocol;
import io.atomix.protocols.raft.ReadConsistency;
import io.atomix.protocols.raft.session.CommunicationStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.annotation.Nullable;

/**
 * Creates the atmoix client and connect to the cluster
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class AtomixConnector extends Thread {

    private static final Logger LOG = LogManager.getLogger(AtomixConnector.class);

    private ProxyProtocol swarmProtocol;
    private ProxyProtocol daemonProtocol;

    private Atomix atomix;
    private AtomixConfig atomixConfig;

    private boolean isConnected = false;

    private List<Node> initialSwarmMembers = new ArrayList<>();

    private Consumer<Atomix> connectCallback;

    public void connect(AtomixConfig config, Consumer<Atomix> connectCallback) {
        this.connectCallback = connectCallback;
        connect(config);
    }

    public void connect(AtomixConfig config) {
        this.atomixConfig = config;

        setName("AtomixBoostrapThread");
        start();
    }

    @Override
    public void run() {
        AtomixBuilder builder = Atomix.builder();

        builder.withMemberId(MemberId.from(atomixConfig.getUuid().toString()))
            .withAddress(atomixConfig.getBindAddress(), atomixConfig.getPort());

        initSwarmMemembers(atomixConfig.getInitialSwarmNodes());

        initMembershipProvider(builder);

        initPartitionGroups(builder);

        LOG.info("Starting Atomix Client...");
        this.atomix = builder.build();
        LOG.info("Atomix Client started");

        initProtocol();
        initConfigService();

        LOG.info("Connecting to cluster...");
        atomix.start().join();
        LOG.info("Connected!");

        isConnected = true;

        postConnect();

        if (this.connectCallback != null) {
            connectCallback.accept(atomix);
        }
    }

    protected void postConnect() {
        Set<Member> members = atomix.getMembershipService().getMembers();
        String names = members.stream().map(member -> member.config().getId().id()).collect(Collectors.joining(", "));
        LOG.info("Connected members: " + names);

        var nodes = atomix.<AtomixNetworkMember>getSet("Nodes").async();
        nodes.add(getCurrentMember());
    }

    /**
     * @return the network member for the current instance of this atomix connector
     */
    protected abstract AtomixNetworkMember getCurrentMember();

    /**
     * Initializes the partition groups of the cluster
     *
     * @param builder the builder used to build the cluster
     */
    protected void initPartitionGroups(AtomixBuilder builder) {
//        builder.addPartitionGroup(PrimaryBackupPartitionGroup.builder("daemon")
//            .withNumPartitions(2)
//            .withMemberGroupStrategy(MemberGroupStrategy.ZONE_AWARE)
//            .build());TODO figure out partition groups...

        builder.addProfile(
            Profile.consensus(initialSwarmMembers.stream().map(node -> node.id().id()).collect(Collectors.toList())));
    }

    /**
     * Initializes the initial swarm members based on the atomix config
     *
     * @param initialSwarmNodes the initial swarm nodes from the config
     */
    protected void initSwarmMemembers(@Nullable List<SwarmNode> initialSwarmNodes) {
        if (initialSwarmNodes == null) {
            initialSwarmNodes = new ArrayList<>();
        }

        for (SwarmNode node : initialSwarmNodes) {
            this.initialSwarmMembers
                .add(Node.builder().withId(node.getId()).withAddress(node.getHost(), node.getPort()).build());
        }

        LOG.info("Found {} initial swarm members", initialSwarmMembers.size());
    }

    /**
     * populates the cluster with known members, acquire full cluster info via 'gossip' protocol
     *
     * @param builder the atomix builder used to boostrap this class
     */
    protected void initMembershipProvider(AtomixBuilder builder) {
        builder.withMembershipProvider(BootstrapDiscoveryProvider.builder()
            .withNodes(initialSwarmMembers)
            .build());
    }

    /**
     * Populates the config service with configs for the used data types
     */
    protected void initConfigService() {

    }

    /**
     * Initializes the protocol used for this cluster
     */
    protected void initProtocol() {
        this.swarmProtocol = MultiRaftProtocol.builder()
            .withReadConsistency(ReadConsistency.LINEARIZABLE)
            .withCommunicationStrategy(CommunicationStrategy.LEADER)
            .build();

        this.daemonProtocol = MultiPrimaryProtocol.builder()
            .withBackups(2)
            .withReplication(Replication.ASYNCHRONOUS)
            .withConsistency(Consistency.EVENTUAL)
            .build();
    }

    public ProxyProtocol getSwarmProtocol() {
        return swarmProtocol;
    }

    public ProxyProtocol getDaemonProtocol() {
        return daemonProtocol;
    }

    @Nullable
    public Atomix getAtomix() {
        return atomix;
    }
}
