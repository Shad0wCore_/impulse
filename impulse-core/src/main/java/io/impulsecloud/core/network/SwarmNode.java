/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.network;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Represents a node of the swarm group, used to boostrap the cluster
 */
public class SwarmNode {

    private String id;
    private String host;
    private int port;

    public SwarmNode(String id, String host, int port) {
        this.id = id;
        this.host = host;
        this.port = port;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SwarmNode node = (SwarmNode) o;
        return port == node.port
            && Objects.equal(id, node.id)
            && Objects.equal(host, node.host);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, host, port);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("id", id)
            .add("host", host)
            .add("port", port)
            .toString();
    }
}
