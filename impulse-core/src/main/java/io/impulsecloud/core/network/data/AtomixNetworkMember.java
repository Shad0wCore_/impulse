/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.network.data;

import io.atomix.utils.net.Address;
import java.util.Objects;
import java.util.UUID;

public class AtomixNetworkMember {

    private String name;
    private UUID id;
    private Address address;
    private Type type;

    public enum Type {
        DAEMON,
        SWARM,
        CLIENT,
    }

    public AtomixNetworkMember(String name, UUID id, Address address,
        Type type) {
        this.name = name;
        this.id = id;
        this.address = address;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public UUID getId() {
        return id;
    }

    public Address getAddress() {
        return address;
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AtomixNetworkMember)) {
            return false;
        }
        AtomixNetworkMember that = (AtomixNetworkMember) o;
        return Objects.equals(name, that.name) &&
            Objects.equals(id, that.id) &&
            Objects.equals(address, that.address) &&
            type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, address, type);
    }

    @Override
    public String toString() {
        return "AtomixNetworkMember{" +
            "name='" + name + '\'' +
            ", id=" + id +
            ", address=" + address +
            ", type=" + type +
            '}';
    }
}
