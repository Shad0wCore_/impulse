package io.impulsecloud.core.server.brand;

import io.impulsecloud.core.MinecraftEdition;
import io.impulsecloud.core.server.ServerRuntime;
import io.impulsecloud.core.server.ServerType;

import com.google.common.base.Objects;

public final class ServerBrand {

    private final String name;
    private final ServerType serverType;
    private final ServerRuntime serverRuntime;
    private final MinecraftEdition minecraftEdition;

    public ServerBrand(String name, ServerType serverType, ServerRuntime serverRuntime,
                       MinecraftEdition minecraftEdition) {
        this.name = name;
        this.serverType = serverType;
        this.serverRuntime = serverRuntime;
        this.minecraftEdition = minecraftEdition;
    }

    public String getName() {
        return this.name;
    }

    public ServerType getServerType() {
        return this.serverType;
    }

    public ServerRuntime getServerRuntime() {
        return this.serverRuntime;
    }

    public MinecraftEdition getMinecraftEdition() {
        return this.minecraftEdition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ServerBrand that = (ServerBrand) o;
        return Objects.equal(name, that.name)
            && serverType == that.serverType
            && serverRuntime == that.serverRuntime
            && minecraftEdition == that.minecraftEdition;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, serverType, serverRuntime, minecraftEdition);
    }

}
