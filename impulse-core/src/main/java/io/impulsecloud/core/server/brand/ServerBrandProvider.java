package io.impulsecloud.core.server.brand;

public interface ServerBrandProvider {

    ServerBrand getServerBrand();

}
