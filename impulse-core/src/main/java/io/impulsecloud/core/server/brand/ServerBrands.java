package io.impulsecloud.core.server.brand;

import io.impulsecloud.core.MinecraftEdition;
import io.impulsecloud.core.server.ServerRuntime;
import io.impulsecloud.core.server.ServerType;

public final class ServerBrands {

    private ServerBrands() {}

    public enum Game implements ServerBrandProvider {

        VANILLA {
            final ServerBrand serverBrand = new ServerBrand("vanilla", ServerType.GAME, ServerRuntime.JVM,
                                                           MinecraftEdition.JAVA_EDITION);
            @Override
            public ServerBrand getServerBrand() {
                return this.serverBrand;
            }
        },
        BUKKIT {
            final ServerBrand serverBrand = new ServerBrand("bukkit", ServerType.GAME, ServerRuntime.JVM,
                                                            MinecraftEdition.JAVA_EDITION);
            @Override
            public ServerBrand getServerBrand() {
                return this.serverBrand;
            }
        },
        NUKKIT {
            final ServerBrand serverBrand = new ServerBrand("nukkit", ServerType.GAME, ServerRuntime.JVM,
                                                            MinecraftEdition.BEDROCK_EDITION);
            @Override
            public ServerBrand getServerBrand() {
                return this.serverBrand;
            }
        },
        POCKET_MINE {
            final ServerBrand serverBrand = new ServerBrand("pocketmine", ServerType.GAME, ServerRuntime.BINARY,
                                                            MinecraftEdition.BEDROCK_EDITION);
            @Override
            public ServerBrand getServerBrand() {
                return this.serverBrand;
            }
        },
        MINET {
            final ServerBrand serverBrand = new ServerBrand("minet", ServerType.GAME, ServerRuntime.BINARY,
                                                            MinecraftEdition.BEDROCK_EDITION);
            @Override
            public ServerBrand getServerBrand() {
                return this.serverBrand;
            }
        },
        CUBERITE {
            final ServerBrand serverBrand = new ServerBrand("cuberite", ServerType.GAME, ServerRuntime.BINARY,
                                                            MinecraftEdition.JAVA_EDITION);
            @Override
            public ServerBrand getServerBrand() {
                return this.serverBrand;
            }
        }

    }

    public enum Proxy implements ServerBrandProvider {

        PROXPROX {
            final ServerBrand serverBrand = new ServerBrand("proxprox", ServerType.PROXY, ServerRuntime.JVM,
                                                            MinecraftEdition.BEDROCK_EDITION);
            @Override
            public ServerBrand getServerBrand() {
                return this.serverBrand;
            }
        },
        BUNGEECORD {
            final ServerBrand serverBrand = new ServerBrand("bungeecord", ServerType.PROXY, ServerRuntime.JVM,
                                                            MinecraftEdition.JAVA_EDITION);
            @Override
            public ServerBrand getServerBrand() {
                return this.serverBrand;
            }
        }

    }

}
