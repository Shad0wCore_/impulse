/*
 *
 *  * Copyright (c) 2018, Impulse Cloud and its contributors
 *  *
 *  * This code is licensed under the MIT license found in the
 *  * LICENSE file in the root directory of this source tree.
 *
 */

package io.impulsecloud.core.util.config;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Allows to load and save config classes.
 *
 * @version 1.0
 * @since 1.0
 */
public interface ConfigParser {

    /**
     * Loads an object of the specified class from the specified path
     *
     * @param path the path of the config file
     * @param clazz the class of the object in the file
     * @param <T> the type of the config object
     * @return the loaded config object
     * @throws RuntimeException if something went wrong
     */
    <T> T load(Path path, Class<T> clazz);

    /**
     * Saves an config object to a config file
     *
     * @param path the path of the config file
     * @param config the config object that should be saved
     * @param <T> the type of the config object
     * @return whether or not it was successful
     */
    <T> boolean save(Path path, T config) throws IOException;

}
