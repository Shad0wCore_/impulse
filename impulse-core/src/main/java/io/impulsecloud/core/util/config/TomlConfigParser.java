/*
 *
 *  * Copyright (c) 2018, Impulse Cloud and its contributors
 *  *
 *  * This code is licensed under the MIT license found in the
 *  * LICENSE file in the root directory of this source tree.
 *
 */

package io.impulsecloud.core.util.config;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.inject.Inject;
import com.moandjiezana.toml.Toml;
import com.moandjiezana.toml.TomlWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A simple {@link ConfigParser} implementation serving the purpose to load and save Toml formatted file
 * configurations.
 *
 * @version 1.0
 * @since 1.0
 */
public class TomlConfigParser implements ConfigParser {

    private static final Logger LOG = LogManager.getLogger(TomlConfigParser.class);

    /**
     * The Toml reader instance used by this config parser
     */
    @Inject
    private Toml tomlReader;

    /**
     * The Toml writer instance used by this config parser
     */
    @Inject
    private TomlWriter tomlWriter;

    @Inject
    private Gson gson;

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T load(Path path, Class<T> clazz) {
        Toml toml = tomlReader.read(path.toFile());
        return gson.fromJson(gson.toJsonTree(toml.toMap()), clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> boolean save(Path path, T config) throws IOException {
        JsonElement json = gson.toJsonTree(config);
        if (path.getParent() == null ||!Files.exists(path.getParent())) {
            LOG.error("Can't save config into non existent dir: {}", path.getParent());
            return false;
        }
        tomlWriter.write(flatten("", json), path.toFile());
        return true;
    }

    /**
     * Recursively tries to flatten the given element using the parent key
     *
     * @param key the key of the parent element
     * @param element the element to flatten
     * @return the flattened element
     */
    private Object flatten(String key, JsonElement element) {
        Map<String, Object> map = new LinkedHashMap<>();

        if (element.isJsonArray()) {
            // list
            JsonArray arr = element.getAsJsonArray();
            List<Object> list = new ArrayList<>();
            // for every list element
            for (int i = 0; i < arr.size(); i++) {
                Map<String, Object> listElement = new LinkedHashMap<>();
                JsonElement jsonElement = arr.get(i);
                // if primitive, add directly
                if (jsonElement.isJsonPrimitive()) {
                    list.add(toObject(jsonElement.getAsJsonPrimitive()));
                } else if (jsonElement.isJsonObject()) {
                    // if object, create map for all entries, flatten if needed
                    JsonObject obj = jsonElement.getAsJsonObject();
                    for (Entry<String, JsonElement> entry : obj.entrySet()) {
                        if (entry.getValue().isJsonPrimitive()) {
                            listElement.put(entry.getKey(), toObject(entry.getValue().getAsJsonPrimitive()));
                        } else {
                            listElement
                                .put(entry.getKey(), flatten(entry.getKey(), entry.getValue()));
                        }
                    }
                } else {
                    // else, just try to flatten
                    listElement.put(key, flatten(key, jsonElement));
                }
                // add map to list, if relevant
                if (listElement.size() != 0) {
                    list.add(listElement);
                }
            }
            // return the converted list
            return list;
        } else if (element.isJsonObject()) {
            // object
            JsonObject obj = element.getAsJsonObject();
            // add every entry to the map, flatten if needed
            for (Entry<String, JsonElement> entry : obj.entrySet()) {
                if (entry.getValue().isJsonPrimitive()) {
                    map.put(entry.getKey(), toObject(entry.getValue().getAsJsonPrimitive()));
                } else {
                    map.put(entry.getKey(), flatten(entry.getKey(), entry.getValue()));
                }
            }
        } else if (element.isJsonPrimitive()) {
            // primitive
            JsonPrimitive prim = element.getAsJsonPrimitive();
            // add directly
            map.put(key, toObject(prim));
        } else {
            throw new IllegalStateException("Illegal json element: " + element);
        }

        return map;
    }

    private Object toObject(JsonPrimitive prim) {
        if (prim.isBoolean()) {
            return prim.getAsBoolean();
        } else if (prim.isNumber()) {
            return prim.getAsNumber();
        } else {
            return prim.getAsString();
        }
    }

}
