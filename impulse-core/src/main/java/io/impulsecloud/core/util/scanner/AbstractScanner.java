package io.impulsecloud.core.util.scanner;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Predicate;

public abstract class AbstractScanner<T> implements Scanner<T> {

    protected static final Predicate<Path> IS_NOT_DIRECTORY_PREDICATE = path -> !Files.isDirectory(path);
    protected static final Predicate<Path> IS_NOT_EMPTY_PREDICATE = path -> path.toFile().length() != 0;

}
