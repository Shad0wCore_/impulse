package io.impulsecloud.core.util.scanner;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;

public interface Scanner<T> {

    Collection<T> scanDirectory(Path directory, boolean recursive) throws IOException;

}
