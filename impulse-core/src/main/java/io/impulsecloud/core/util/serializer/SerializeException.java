package io.impulsecloud.core.util.serializer;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class SerializeException extends Exception {

    private final ActionType actionType;

    public SerializeException(ActionType actionType, String message) {
        super(message);
        this.actionType = actionType;
    }

    public SerializeException(ActionType actionType, String message, Throwable cause) {
        super(message, cause);
        this.actionType = actionType;
    }

    public SerializeException(ActionType actionType, Throwable cause) {
        super(cause);
        this.actionType = actionType;
    }

    public ActionType getActionType() {
        return actionType;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(actionType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SerializeException that = (SerializeException) o;
        return actionType == that.actionType;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("actionType", actionType)
            .toString();
    }

    public enum ActionType {

        LOAD,
        SAVE

    }

}
