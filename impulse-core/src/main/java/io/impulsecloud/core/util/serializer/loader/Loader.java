package io.impulsecloud.core.util.serializer.loader;

import io.impulsecloud.core.util.serializer.SerializeException;

import java.util.Optional;

public interface Loader<A, B> {

    Optional<A> load(B source) throws SerializeException;

}
