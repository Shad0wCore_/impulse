package io.impulsecloud.core.util.serializer.saver;

import io.impulsecloud.core.util.serializer.SerializeException;

public interface Saver<A, B> {

    void save(A input, B destination) throws SerializeException;

}
