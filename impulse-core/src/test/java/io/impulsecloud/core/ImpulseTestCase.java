/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core;

import io.impulsecloud.core.config.GsonTestObject;
import io.impulsecloud.core.config.TestTypeAdapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Injector;

/**
 * Base class for all test classes
 *
 * @version 1.0
 * @since Release 'Overdrive'
 */
public abstract class ImpulseTestCase {

    private Injector injector;

    public ImpulseTestCase() {
        injector = new TestGuiceModule().createInjector();
    }

    public Injector getInjector() {
        return injector;
    }

    class TestGuiceModule extends ImpulseGuiceModule {

        @Override
        protected Gson getGson() {
            return new GsonBuilder().registerTypeAdapter(
                GsonTestObject.class, new TestTypeAdapter()).create();
        }
    }

}
