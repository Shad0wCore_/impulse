/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.config;

import java.util.List;

class ComplexObject {

    TestObject object;
    List<TestObject> list;
    String string;
    ComplexObject nested;

    ComplexObject(TestObject object,
                  List<TestObject> list, String string, ComplexObject nested) {
        this.object = object;
        this.list = list;
        this.string = string;
        this.nested = nested;
    }

}