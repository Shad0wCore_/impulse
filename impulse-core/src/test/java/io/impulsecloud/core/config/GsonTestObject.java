/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.config;

public class GsonTestObject {

    String s;
    int i;
    double d;

    public GsonTestObject(String s, int i, double d) {
        this.s = s;
        this.i = i;
        this.d = d;
    }

}