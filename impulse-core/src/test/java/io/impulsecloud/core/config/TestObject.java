/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.config;

import java.util.Objects;

class TestObject {

    String string;
    int integer;

    TestObject(String string, int integer) {
        this.string = string;
        this.integer = integer;
    }

    @Override
    public int hashCode() {
        return Objects.hash(string, integer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestObject)) {
            return false;
        }
        TestObject that = (TestObject) o;
        return integer == that.integer
            && Objects.equals(string, that.string);
    }

}
