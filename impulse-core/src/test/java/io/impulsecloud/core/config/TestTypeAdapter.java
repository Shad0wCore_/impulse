/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.config;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class TestTypeAdapter extends TypeAdapter<GsonTestObject> {

    @Override
    public void write(JsonWriter out, GsonTestObject value) throws IOException {
        out.beginObject().name("test").value(value.s + ":" + value.i + ":" + value.d).endObject();
    }

    @Override
    public GsonTestObject read(JsonReader in) throws IOException {
        in.beginObject();
        String name = in.nextName();
        String[] args = in.nextString().split(":");
        in.endObject();
        return new GsonTestObject(args[0], Integer.parseInt(args[1]), Double.parseDouble(args[2]));
    }

}
