/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.core.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import io.impulsecloud.core.ImpulseTestCase;
import io.impulsecloud.core.util.config.TomlConfigParser;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class TomlConfigParserTest extends ImpulseTestCase {

    private TomlConfigParser classUnderTest;

    @BeforeEach
    void setUp() {
        classUnderTest = getInjector().getInstance(TomlConfigParser.class);
    }

    @Test
    void load_simpleObject_loadedValues() throws Exception {
        // arrange
        Path path = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("simpleObject-load.toml")).toURI());

        // action
        TestObject object = classUnderTest.load(path, TestObject.class);

        // assert
        assertThat(object.integer).isEqualTo(42);
        assertThat(object.string).isEqualTo("TestString");
    }

    @Test
    void load_complexObject_loadedValues() throws Exception {
        // arrange
        Path path = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("complexObject-load.toml")).toURI());

        // action
        ComplexObject object = classUnderTest.load(path, ComplexObject.class);

        // assert
        assertThat(object.string).isEqualTo("COMPLEX");
        assertThat(object.list).isNotEmpty();
        assertThat(object.list).contains(new TestObject("1", 1), new TestObject("2", 2));
        assertThat(object.object.string).isEqualTo("Test123");
        assertThat(object.object.integer).isEqualTo(1337);
        assertThat(object.nested.string).isEqualTo("NESTED");
        assertThat(object.nested.object.integer).isEqualTo(4);
    }

    @Test
    void load_gsonObject_loadedValues() throws Exception {
        // arrange
        Path path = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("gson-load.toml")).toURI());

        // action
        GsonTestObject object = classUnderTest.load(path, GsonTestObject.class);

        // assert
        assertThat(object.s).isEqualTo("Test");
        assertThat(object.i).isEqualTo(2);
        assertThat(object.d).isEqualTo(2.6);
    }

    @Test
    void load_nonExistentFile_throwsRuntimeException() {
        assertThrows(RuntimeException.class, () -> classUnderTest.load(Paths.get("test"), ComplexObject.class));
    }

    @Test
    void save_simpleObject_wroteValues() throws Exception {
        // arrange
        Path savePath = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("simpleObject-save.toml")).toURI());
        Path loadPath = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("simpleObject-load.toml")).toURI());
        TestObject testObject = new TestObject("TestString", 42);

        // action
        boolean success = classUnderTest.save(savePath, testObject);

        // assert
        assertThat(success).isTrue();
        assertThat(savePath.toFile()).hasSameContentAs(loadPath.toFile());
    }

    @Test
    void save_complexObject_wroteValues() throws Exception {
        // arrange
        Path savePath = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("complexObject-save.toml")).toURI());
        Path loadPath = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("complexObject-load.toml")).toURI());
        ComplexObject testObject = new ComplexObject(
            new TestObject("Test123", 1337),
            Lists.newArrayList(new TestObject("1", 1), new TestObject("2", 2)),
            "COMPLEX",
            new ComplexObject(
                new TestObject("4", 4),
                new ArrayList<>(),
                "NESTED",
                null));

        // action
        boolean success = classUnderTest.save(savePath, testObject);

        // assert
        assertThat(success).isTrue();
        assertThat(savePath.toFile()).hasSameContentAs(loadPath.toFile());
    }

    @Test
    void save_gsonObject_wroteValues() throws Exception {
        // arrange
        Path savePath = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("gson-save.toml")).toURI());
        Path loadPath = Paths
            .get(Objects.requireNonNull(getClass().getClassLoader().getResource("gson-load.toml")).toURI());
        GsonTestObject testObject = new GsonTestObject("Test", 2, 2.6);

        // action
        boolean success = classUnderTest.save(savePath, testObject);

        // assert
        assertThat(success).isTrue();
        assertThat(savePath.toFile()).hasSameContentAs(loadPath.toFile());
    }

    @Test
    void save_nonExistentDir_successFalse() {
        // action
        boolean success = false;

        try {
            success = classUnderTest.save(Paths.get("/unknowndir/test"), new HashMap<>());
        } catch (IOException e) {
            fail(e);
        }

        // assert
        assertThat(success).isFalse();
    }

}
