/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.daemon;

import io.impulsecloud.core.network.AtomixConnector;
import io.impulsecloud.core.network.data.AtomixNetworkMember;
import io.impulsecloud.core.network.data.AtomixNetworkMember.Type;

import io.atomix.utils.net.Address;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DaemonAtomixConnector extends AtomixConnector {

    @Inject
    private DaemonConfig config;

    @Override
    protected AtomixNetworkMember getCurrentMember() {
        return new AtomixNetworkMember(
            config.getDisplayName(),
            config.getNetworkConfig().getUuid(),
            Address.from(config.getNetworkConfig().getBindAddress(), config.getNetworkConfig().getPort()),
            Type.DAEMON);
    }
}
