/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.daemon;

import io.impulsecloud.core.network.AtomixConfig;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Config file used to configure the daemon
 *
 * @version 1.0
 * @since 1.0
 */
public class DaemonConfig {

    private String displayName;
    private AtomixConfig networkConfig;

    public DaemonConfig(String displayName, AtomixConfig networkConfig) {
        this.displayName = displayName;
        this.networkConfig = networkConfig;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public AtomixConfig getNetworkConfig() {
        return networkConfig;
    }

    public void setNetworkConfig(AtomixConfig networkConfig) {
        this.networkConfig = networkConfig;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("displayName", displayName)
            .add("networkConfig", networkConfig)
            .toString();
    }
}
