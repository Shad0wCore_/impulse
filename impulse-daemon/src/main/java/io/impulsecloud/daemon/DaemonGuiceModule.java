/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.daemon;

import io.impulsecloud.core.ImpulseGuiceModule;
import io.impulsecloud.core.network.AtomixConnector;

public class DaemonGuiceModule extends ImpulseGuiceModule {

    private DaemonConfig config;

    public DaemonGuiceModule(DaemonConfig config) {
        this.config = config;
    }

    @Override
    protected void configure() {
        super.configure();

        bind(AtomixConnector.class).to(DaemonAtomixConnector.class).asEagerSingleton();

        bind(DaemonConfig.class).toInstance(config);
    }

}
