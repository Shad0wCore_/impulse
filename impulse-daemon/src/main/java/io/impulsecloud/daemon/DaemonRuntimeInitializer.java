/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.daemon;

import io.impulsecloud.core.CoreRuntimeInitializer;

import com.google.inject.Injector;
import java.nio.file.Path;
import javax.inject.Inject;

/**
 * Runtime initializer for the daemon
 *
 * @version 1.0
 * @since 1.0
 */
public class DaemonRuntimeInitializer extends CoreRuntimeInitializer {

    @Inject
    private DaemonAtomixConnector atomixConnector;

    private Path configPath;
    private DaemonConfig config;

    public DaemonRuntimeInitializer(Path configPath) {
        this.configPath = configPath;
    }

    @Override
    public void loadConfig() {
        config = configParser.load(configPath, DaemonConfig.class);

        replaceDefaultUUID(config, config.getNetworkConfig(), configPath);
    }

    @Override
    public Injector createInjector() {
        return new DaemonGuiceModule(config).createInjector();
    }

    @Override
    protected void initAtomix() {
        atomixConnector.connect(config.getNetworkConfig());
    }
}
