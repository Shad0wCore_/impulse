/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import io.atomix.cluster.MemberId;
import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;
import io.atomix.core.counter.AsyncAtomicCounter;
import io.atomix.core.list.AsyncDistributedList;
import io.atomix.core.list.DistributedListConfig;
import io.atomix.core.profile.Profile;
import io.atomix.core.value.AsyncAtomicValue;
import io.atomix.protocols.raft.MultiRaftProtocol;
import io.atomix.protocols.raft.MultiRaftProtocolConfig;
import io.atomix.protocols.raft.ReadConsistency;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

// CHECKSTYLE:OFF
public class Test {

    public static void main(String[] args) {
        new Thread(() -> new Test().startClient("member1", 5001)).start();
        sleep(2000);
        new Thread(() -> new Test().startClient("member2", 5002)).start();
//        new Thread(() -> new Test().startClient("member3", 5003)).start();
//        new Thread(() -> new Test().startClient("otto", 5004)).start();
//        new Thread(() -> new Test().startClient("swarm1", 5000)).start();
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private String name;

    public void startClient(String member, int port) {
        Thread.currentThread().setName(member + "-THREAD");
        this.name = member;

//        new Thread(() -> {
//            while (true) {
//                log("heartbeat");
//                sleep(5000);
//            }
//        }).start();

        AtomixBuilder builder = Atomix.builder();

        builder.withMemberId(MemberId.from(member))
            .withAddress("localhost", port);

        builder.withMembershipProvider(BootstrapDiscoveryProvider.builder()
                                           .withNodes(
                                               Node.builder()
                                                   .withId("member1")
                                                   .withAddress("localhost:5001")
                                                   .build(),
                                               Node.builder()
                                                   .withId("member2")
                                                   .withAddress("localhost:5002")
                                                   .build(),
                                               Node.builder()
                                                   .withId("member3")
                                                   .withAddress("localhost:5003")
                                                   .build())
                                           .build());

//        builder.withManagementGroup(RaftPartitionGroup.builder("management")
//            .withNumPartitions(1)
//            .withStorageLevel(StorageLevel.MEMORY)
//            .withMembers("member1", "member2", "member3")
//            .build());

//        builder.addPartitionGroup(RaftPartitionGroup.builder("swarms")
//            .withNumPartitions(3)
//            .withStorageLevel(StorageLevel.MEMORY)
//            .withMembers("member1", "member2", "member3")
//            .build());

        builder.addProfile(Profile.consensus("member1", "member2", "member3"));

        MultiRaftProtocol protocol = MultiRaftProtocol.builder()
            .withReadConsistency(ReadConsistency.LINEARIZABLE)
            .build();

        log("Starting client " + name);
        Atomix atomix = builder.build();
//        DistributedListConfig config = new DistributedListConfig();
//        config.setProtocolConfig(new MultiRaftProtocolConfig());
//        config.setName("TEST");
//        atomix.getConfigService().addConfig(config);

        log("Connecting to cluster....");
        atomix.start().join();
        log("Connected");

        AsyncAtomicCounter counter = atomix.getAtomicCounter("Test").async();
        if (name.equals("member1")) {
            counter.addAndGet(2).thenRun(() -> log("Incremented counter"));
        }
        counter.get().thenAccept(c -> log("Counter is " + c));

        counter.addStateChangeListener(primitiveState -> log(primitiveState.name()));
//
//        AsyncAtomicValue<String> test2 = atomix.<String>getAtomicValue("Test2").async();
//        test2.addListener(event -> log("Test2 was changed from " + event.oldValue() + " to " + event.newValue()));
//
//        if (name.equals("member1")) {
//            while (true) {
//                log("set test");
//                test2.set("Test" + (int) (Math.random() * 100));
//                sleep(1000);
//                log("members " + atomix.getMembershipService().getReachableMembers().stream().map(m -> m.id().id())
//                    .collect(Collectors.joining(" ")));
//            }
//        }
//
//        if (name.equals("otto")) {
//            test2.set("OTTO");
//        }
//
//        AsyncDistributedList<TestClass> list = atomix.<TestClass>listBuilder("test-list").withProtocol(protocol)
//            .withElementType(TestClass.class).build().async();
//        list.add(new TestClass("1", 1));
//        list.add(new TestClass("2", 2));
//        list.addListener(event -> log(event.type().name() + " " + event.element()));
//
//        list.iterator().next().thenAccept(testClass -> log("GOT " + testClass));
    }

    private void log(String msg) {
        System.out.println(LocalTime.now().format(DateTimeFormatter.ISO_TIME) + " [" + name + "] " + msg);
    }

    class TestClass {

        String test;
        int test2;

        TestClass(String test, int test2) {
            this.test = test;
            this.test2 = test2;
        }

        @Override
        public String toString() {
            return "TestClass{"
                + "test='"
                + test + '\''
                + ", test2=" + test2
                + '}';
        }
    }
}
//CHECKSTYLE:ON
