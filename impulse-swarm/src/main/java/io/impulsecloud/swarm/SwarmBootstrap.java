/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.swarm;

import io.impulsecloud.core.CoreBootstrap;
import io.impulsecloud.core.util.ValueConverters;

import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Impulse Daemon bootstrap implementation
 *
 * @version 1.0
 * @since 1.0
 */
public class SwarmBootstrap extends CoreBootstrap {

    public static void main(String[] args) {
        new SwarmBootstrap(args);
    }

    private OptionSpec<Path> configFileOption;

    public SwarmBootstrap(String[] args) {
        super(args);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configureOptionParser() {
        super.configureOptionParser();

        configFileOption = this.optionParser.accepts("configFile", "Specifies the config file that should be used")
            .withRequiredArg().withValuesConvertedBy(ValueConverters.PATH);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean bootstrap(OptionSet optionSet) {
        if (super.bootstrap(optionSet)) {
            return true;
        }

        Path configFilePath = optionSet.valueOf(configFileOption);
        if (configFilePath == null) {
            configFilePath = Paths.get("swarm.toml");
        }

        new SwarmRuntimeInitializer(configFilePath).initialize();

        return true;
    }

}
