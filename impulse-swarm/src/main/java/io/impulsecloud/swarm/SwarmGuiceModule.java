/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.swarm;

import io.impulsecloud.core.ImpulseGuiceModule;
import io.impulsecloud.core.network.AtomixConnector;

public class SwarmGuiceModule extends ImpulseGuiceModule {

    private SwarmConfig config;

    public SwarmGuiceModule(SwarmConfig config) {
        this.config = config;
    }

    @Override
    protected void configure() {
        super.configure();

        bind(AtomixConnector.class).to(SwarmAtomixConnector.class).asEagerSingleton();

        bind(SwarmConfig.class).toInstance(config);
    }

}
