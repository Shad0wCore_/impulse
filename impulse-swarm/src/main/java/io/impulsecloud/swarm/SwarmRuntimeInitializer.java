/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.swarm;

import io.impulsecloud.core.CoreRuntimeInitializer;
import io.impulsecloud.swarm.services.MembershipService;
import io.impulsecloud.swarm.services.Service;

import com.google.inject.Injector;
import java.nio.file.Path;
import java.util.HashSet;
import javax.inject.Inject;

/**
 * Runtime initializer for the swarm,
 *
 * @version 1.0
 * @since 1.0
 */
public class SwarmRuntimeInitializer extends CoreRuntimeInitializer {

    @Inject
    private SwarmAtomixConnector atomixConnector;
    @Inject
    private Injector injector;

    private Path configPath;
    private SwarmConfig config;

    public SwarmRuntimeInitializer(Path configPath) {
        this.configPath = configPath;
    }

    @Override
    public void loadConfig() {
        config = configParser.load(configPath, SwarmConfig.class);

        replaceDefaultUUID(config, config.getNetworkConfig(), configPath);
    }

    @Override
    public Injector createInjector() {
        return new SwarmGuiceModule(config).createInjector();
    }

    @Override
    protected void initAtomix() {
        atomixConnector.connect(config.getNetworkConfig(), (atomix) -> initServices());
    }

    private void initServices() {
        var services = new HashSet<Class<? extends Service>>();
        services.add(MembershipService.class);

        for (var service : services) {
            injector.getInstance(service).init();
        }
    }
}
