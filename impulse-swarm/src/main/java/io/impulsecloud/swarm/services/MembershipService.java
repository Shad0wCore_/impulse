/*
 * Copyright (c) 2018, Impulse Cloud and its contributors
 *
 * This code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

package io.impulsecloud.swarm.services;

import io.impulsecloud.core.network.AtomixConnector;
import io.impulsecloud.core.network.data.AtomixNetworkMember;

import io.atomix.cluster.ClusterMembershipEvent.Type;
import io.atomix.cluster.ClusterMembershipService;
import io.atomix.core.Atomix;
import io.atomix.core.set.AsyncDistributedSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MembershipService implements Service {

    private static final Logger LOG = LogManager.getLogger(MembershipService.class);

    @Inject
    private AtomixConnector connector;

    @Override
    public void init() {
        LOG.info("Initializing...");
        Atomix atomix = connector.getAtomix();
        if (atomix == null) {
            LOG.error("Could not initialize MembershipService since atmoix isn't connected!");
            return;
        }

        var nodes = connector.getAtomix().<AtomixNetworkMember>getSet("Nodes").async();
        atomix.getMembershipService().addListener(event -> {
            if (event.type() == Type.MEMBER_REMOVED || event.type() == Type.REACHABILITY_CHANGED) {
                if (event.subject().isReachable()) {
                    return;
                }

                LOG.info("Lost connection to member {}, removing from membership service.", event.subject().id().id());
                nodes.stream()
                    .filter(member -> member.getId().toString().equalsIgnoreCase(event.subject().id().id()))
                    .forEach(nodes::remove);

                nodes.size().thenAccept(size -> {
                    if (size != atomix.getMembershipService().getReachableMembers().size()) {
                        cleanup(atomix.getMembershipService(), nodes);
                    }
                });
            }
        });
    }

    private void cleanup(ClusterMembershipService membershipService,
        AsyncDistributedSet<AtomixNetworkMember> nodes) {
        LOG.info("Running cleanup...");
        nodes.stream()
            .filter(member ->
                membershipService.getReachableMembers().stream()
                    .noneMatch(m -> m.id().id().equalsIgnoreCase(member.getId().toString())))
            .forEach(nodes::remove);
    }
}
